//Ajoute "l'interface" en important un objet javascript, contenu dans le fichier noté.
var module1 = require("./lanceurinitiale.js")

//Pour executer une fonction qui a été importée, il suffit de la donner
module1 //Si on a importé juste une fonction
module1.CLE(param1,param2) //Si on a importé un objet qui contient plusieurs fonctions, avec des paramètres
module1.TOTO() //Si on a importé plusieurs fonction dans un objet, sans paramètres
try {
  // Lecture fichierA
  var contenuA = readFileSync("blabla1"); // peut lever une exception
  // Lecture fichierB
  var contenuB = readFileSync("blabla2"); // peut lever une exception
  // Concaténation
  var contenuC = contenuA + '\n' + contenuB;
  // Écriture fichierC
  writeFileSync("blabla", contenuC); // peut lever une exception
  // Succès :)
  console.log('OK');
} catch (err) {
  // Erreur
  console.log('FAIL: ' + err.message);
}

function readFileSync(fichier){
	console.log(fichier)
	return "test"
}

function writeFileSync(fichier){
	console.log("test")
}

function readFileSync(fichier){
	console.log("test")
}

function readFileSync(fichier){
	console.log("test")
}

//PAS SOLUTION
	//The set of functions that I want to call in order
	function getData(initialData) {
	  //gets the data
	console.log('First Step --> ');
	console.log(test);
	
	  return new Promise(function (resolve, reject) {
	    resolve('Hello World!')
	  })
	}

	function parseData(dataFromGetDataFunction) {
	  //does some stuff with the data
	  apiSpotlight.annotate(textToAnnoter,function (jsonAnnotations) {
			console.log('Second Step --> 1 ');
			console.log(test);
			test = miseEnForme(jsonAnnotations, cb);
			console.log('Second Step --> 2 ');
			console.log(test);
			return new Promise(function (resolve, reject) {
		    	resolve('Hello World!')
		  	})
		});
	}

	function validate(dataFromParseDataFunction) {
	  //validates the data
		console.log('Fourth Step --> ');
		console.log(test);
	
	  return new Promise(function (resolve, reject) {
	    resolve('Hello World!')
	  })
	}

	//The function that orchestrates these calls 
	console.log(getData(test).then(parseData).then(validate))
	
	
	
//VRIA SOLUTION
var test = {}
	async.series([
	    function (callback) {
		console.log('First Step --> ');
		console.log(test);
		callback(null, '1');
	    },
	    function (callback) {
		console.log('Second Step --> ');
		//Execute une fonction asynchrone qui fait un callback
		module.fonctionAsynchrone(textToAnnoter,function (jsonAnnotations) {
	//Le callback est executé (on peut appeller d'autres fonctions, faire ce qu'on veut)
			test = miseEnForme(jsonAnnotations, cb);
			console.log(test);
			//On appelle la troisième fonction
			callback(null, '2');
		});
	    },
	    function (callback) {
		console.log('Third Step --> ');
		console.log(test);
	    }
	],
	function (err, result) {
	    console.log(result);
	});
	
	
	
	
	//SOLUTION THEORIQUE
	
		var test = {}
	
	async.series([
	    function (callback) {
		console.log('First Step --> ');
		console.log(test);
		callback(null, '1');
	    },
	    function (callback) {
		console.log('Second Step --> ');
		//RÃ©cupÃ©rer les singulets 
		apiSpotlight.annotate(textToAnnoter,function (jsonAnnotations) {
			console.log('Second Step --> 1 ');
			console.log(test);
			test = miseEnForme(jsonAnnotations, cb);
			console.log('Second Step --> 2 ');
			console.log(test);
			callback(null, test);
		});
	    },
	    function (callback, test) {
		console.log('Third Step --> ');
		console.log(test);
		callback(null, "3");
	    }
	],
	function (err, result) {
		console.log(result);
		return result
	});
	
	console.log('Fourth Step --> ');
	console.log(test);
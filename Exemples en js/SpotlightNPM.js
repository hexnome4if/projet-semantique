var mlspotlight = require('dbpedia-spotlight');
const jq = require('node-jq') //VOIR : https://github.com/sanack/node-jq


input="Donald Trump, né le 14 juin 1946 à New York, est un homme d'affaires américain. Magnat milliardaire de l'immobilier, il est également animateur de télévision et homme politique"
//input = "My text to be analyzed."
//mlspotlight.annotate(input,function(output){
//    console.log(output);
//});

//fix to a specific endpoint (i.e. disabling language detection) 
mlspotlight.fixToEndpoint('english');
  
//use custom endpoints 
  mlspotlight.configEndpoints(
    {
      "english": {
      protocol:'http:',
      host:'model.dbpedia-spotlight.org',
      path:'/en/annotate',
      port:'80',
      confidence:0.2
      }
    }
  );
  
  jq.run('.', '{ foo: "bar" }', { input: 'string', output : 'string' }).then(console.log)
  
mlspotlight.annotate(input,function(output){
  //Raw output print
  console.log(output);
  //output.response.Resources est la liste des ressources renvoyées par l'API web
  //console.log(output.response.Resources[0])
  var res = output.response.Resources;
  var valOutput = [];
  
  //Les informations qu'on veut retirer de l'objet
  var ensembleValuesToParse = ['@URI','@types','@surfaceForm']
  
  for (var i in res) { //Pour chaque Entité retournée
      	valOutput[i] = {} //Initialisation de l'objet
  	for (var j in ensembleValuesToParse) { //Pour chaque propriété qu'on veut extraire
  		//DEBUG // Affichage durant le parsing // console.log('Parsing de : ' + ensembleValuesToParse[j] + " en num " + j)
  		valOutput[i][ensembleValuesToParse[j]] = res[i][ensembleValuesToParse[j]] //On prend la valeur URI et on la range 'à la main'. Notez la bidouille à cause du caractères spécial
  	}
  }
  // DEBUG // Affichage du json de sortie // 
  console.log(valOutput)
  return valOutput
});
  
  
//unfix endpoint (i.e. enabling language detection) 
mlspotlight.unfixEndpoint();



// ===================================================

  
  //jq.run('.',output, {input: 'string'}).then(console.log) //jq.run(filter, jsonPath, options)

// {
//   "foo": "bar"
// }
	//var monJson = JSON.parse(output);
	/*
	var listeObj = output['response']
    console.log(output['Resources'])
    console.log(listeObj["@URI"])
  /*
  var test = JSON.stringify(output)
  var obj = JSON.parse(test);

   console.log(obj);
   */
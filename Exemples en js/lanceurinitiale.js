// Importation des librairaires
var sys = require('sys')
var exec = require('child_process').exec;
//2 modes d'exceution : exec et spawn, deux comportements différents.
//Spwan "meilleur" : par habitude.
//Exec: on peut lancer un .bat sous windows

function puts(error, stdout, stderr) { 
	sys.puts(stdout) 
}

function affichageConsole(err, stdout, stderr) {
	console.log(stdout)
}

function exectest(param1, param2) {
	exec("QUERY=michelle+obama OFFSET=10 ./queryGoogle.sh", affichageConsole);
}

// ATTENTION ! Il faut qu'un seul export par fichier ! On défini l'interface avec les autres modules.
//module.exports = exectest; //Ici on exporte une seule fonction 
module.exports = {CLE:exectest, TOTO:affichageConsole} //ici on exporte deux fonctions, on les mets donc dans un seul objet.
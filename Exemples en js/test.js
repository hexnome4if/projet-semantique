var LIMIT_INF_TAILLE_TEXT = 10

function test(error, stdout, stderr) {
	//DEBUG // Afficher directement la sortie donnée à la fonction // console.log(stdout)
	result = stdout.split("\n");
	for (var i in result) {
		//DEBUG // Affichage de la ligne et de l'élement du tableau // console.log(i + "/" + countWords(result[i]) + ": " + result[i]) + "\n";
		if(countWords(result[i]) < LIMIT_INF_TAILLE_TEXT){
			result.splice(i,1) // On retire l'élément si il est plus petit que la taille minimal de mot requis.
		}
	}
	//DEBUG // Afficher toutes les lignes parsées // 
	console.log(result)
}

function countWords(str) {
  return str.trim().split(/\s+/).length; //Note : on retire les espaces avant le premier mot et après le dernier mot, avec trim
}

test('', "          Ceci est une grosse blague", '')
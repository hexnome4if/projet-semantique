var cheminAccesScripts = "scripts_utilities/"

//Ajoute "l'interface" en important un objet javascript, contenu dans le fichier noté.
var searchGoogle = require("./" + cheminAccesScripts + "SearchGoogle.js")
var dereferenceURL = require("./" + cheminAccesScripts + "dereferenceurURL.js")
var spotLight = require("./" + cheminAccesScripts + "spotLight.js")
var proprietes = require("./" + cheminAccesScripts + "getProperties.js")
var sync = require("./" + cheminAccesScripts + "Synchrone.js")

var util = require('util');

var CONFIDENCE = "0.7"
var SUPPORT = "10"


// On récupère une toutes les relations ?p (ensemble) entre deux entités ?s et ?o, soit ?s ?p ?o
// Renvoie un set contenant les relations
function getRelationsBetweenEntities(s, o)
{
	// On crée le set qui contiendra les relations
	var unSetCase = new Set();

	// Afin de rendre l'exécution synchrone, on passe par le système
	var commandeToExecute =
	"var SparqlClient = require('sparql');\
	var endpoint = 'https://dbpedia.org/sparql';\
	var client = new SparqlClient.Client(endpoint);\
	var query = 'select distinct * where {\<" + s + "\> ?r \<" + o + "\> \} LIMIT 100';\
	client.query(query,function(err,result){\
	result = JSON.stringify(result);\
	console.log(result);\
	});";
	var result = sync.executeCommandeSync(commandeToExecute)


	// On transforme le résultat (string) en json
	result = JSON.parse(result)

	for(const data in result.results.bindings)
	{
		// On ajoute chaque relation au set
		// Si une relation est déjà présente, elle ne sera pas ajoutée car set -> ensemble
		unSetCase.add(result.results.bindings[data].r.value);
	}

	return unSetCase;
}

// On construit la matrice contenant les relations entre entités.
// Une case contient un set des relations.
function constructMatrixOfRelations(entites)
{
	// Matrice qui contiendra les ensembles de relations ?p entre ?s et ?o
	// ligne : ?s
	// colonne : ?o
	matrix_links = [];

	// Json qui contiendra les triplets
	triplets = "[]";
	jsonTriplets = JSON.parse(triplets);


	for(const i in entites)
	{
		matrix_links.push([]);
		for(const j in entites)
		{
			var setRelations = getRelationsBetweenEntities(entites[i],entites[j]);
			//console.log(setRelations);
			matrix_links[i].push(setRelations);
			for (let relation of setRelations)
			{
				var triplet = {
                "s": entites[i].substring(entites[i].lastIndexOf("/") +1),
                "p": relation.substring(relation.lastIndexOf("/") +1),
                "o": entites[j].substring(entites[j].lastIndexOf("/") +1)
				};

				jsonTriplets.push(triplet);
				//jsonTriplets = JSON.parse(jsonTriplets);
				//console.log(triplet);
			}
		}
	}
	return [matrix_links, jsonTriplets];
}

// Renvoie le rapport entre le nombre de relations existant entre les
// différentes entités et le nombre maximum de relations possibles.
// (Au maximum on a i^2 relations mais on exclut les relations d'une entité
// avec elle-même).
function calculateLinksBetweenConcepts(matrix)
{
	var maxPossibleLinks = 0;
	var actualLinks = 0;
	// Si on ne dispose que d'une entité, ou aucune, l'information n'est pas
	// confirmée.
	if(matrix.length<=1)
	{
		return 0;
	}
	// Sinon on parcourt la matrice
	for(i=0;i<matrix.length;i++)
	{
		for(j=0;j<matrix[i].length;j++)
		{
			if(matrix[i][j].size!=0)
			{
				// Même s'il existe plusieurs relations entre deux entités, on
				// en considère une seule
				actualLinks++;
			}
			// On exclut la relation d'une entité avec elle-même
			if(i!=j)
			{
				maxPossibleLinks++;
			}
		}
	}
	return actualLinks/maxPossibleLinks;
}


function mainSparql(text)
{
	// On récupère les entités de la phrases à l'aide de spotlight
	var entites = spotLight.getEntitiesSync(text,CONFIDENCE,SUPPORT);
	console.log("********************* ENTITES **********************");
	console.log(entites);
	console.log("****************************************************");

	// On récupère
	var resultArrays = constructMatrixOfRelations(entites);
	var matrix_links = resultArrays[0];
	var jsonTriplets = resultArrays[1];
	var fraction = calculateLinksBetweenConcepts(matrix_links);

	// On affiche les résultats dans la console
	console.log("********************* MATRICE DES RELATIONS *********************");
	console.log(matrix_links);
	console.log("*****************************************************************");
	console.log("********************* TRIPLETS **********************");
	console.log(jsonTriplets);
	console.log("*****************************************************");
}


var text = "Barack Obama is married to Michelle Obama, has a dog named Rex and two daughters, Laura and Malia, and he lives in Paris.";
mainSparql(text);

module.exports = {constructMatrixOfRelations:constructMatrixOfRelations}

var execSync = require('child_process').execSync;

/*
// EXEMPLE INITIAL DE COMMANDE ASYNCHRONE 
apiSpotlight.annotate(texte,function (jsonAnnotations) {
	console.log(jsonAnnotations);
} );
*/


function executeCommandeSync(commande){
	//NOTE : il faut que la commande soit une string (entourée de "") et ne contienne pas de "
	commande = sanitizeExternQuotes(commande);
	var sortie = execSync("node -e \"" + commande + "\"").toString();

	/* SI POTENTIEL PROBLEMES DE JSON, POUR DEBUGGER
	sortie = JSON.stringify(sortie)
	sortie = JSON.parse(sortie)
	console.log("sortie :")
	console.log(sortie)
	console.log("sortie response :")
	console.log(sortie.response)
	*/
	
	return sortie;
}

function getSTDout(error, stdout, stderr) {
	result = stdout.split("\n"); //On sépare le texte sur les sauts de ligne
	
	for (var i in result) {
		result[i] = result[i].trim()
		if(countWords(result[i]) < LIMIT_INF_TAILLE_TEXT){
			result.splice(i,1) // On retire l'élément si il est plus petit que la taille minimal de mot requis.
		}
	}
	//DEBUG // Afficher toutes les lignes parsées // console.log(result)
	return result;
}

function sanitizeExternQuotes(string){
	var newString = string.toString().replace(/\"/g, " ")
	// DEBUG // 
	
	if(newString != string){
		console.error(">> ATTENTION ! DES GUILLEMETS ont été retirés  de la commande ! <<")
		console.error(">> Commande actuellement executée :")
		console.error(newString)
	}
	
	return newString
}


// ===== EXEMPLES ======
function sanitize(string){
	var newString = string.toString().replace(/\'/g, " ")
	// DEBUG // console.log(newString)
	return newString
}


function exemple(){

	var texteHard = 'Donald Trump, né le 14 juin 1946 à New York, est un homme d\'affaires américain. Magnat milliardaire de l\'immobilier, il est également animateur de télévision et homme politique'
	var texteSimple = "Donald Trump, né le'  14 juin 1946 à New York"
	//var commande = "console.log('toto')"
	// 
	// ======== Solution 1 : executer une commande ======== 
	//NOTE : il faut que la commande soit entre "" et ne contienne pas de "" 
	var commandeParamEnDur = "var apiSpotlight = require('dbpedia-spotlight'); \
			apiSpotlight.annotate('toto',function (jsonAnnotations) {\
				console.log(jsonAnnotations)\
			;} );"
	var commandeParamEnVar = "var apiSpotlight = require('dbpedia-spotlight'); \
			apiSpotlight.annotate(' " + sanitize(texteSimple) + " ',function (jsonAnnotations) {\
				console.log(jsonAnnotations)\
			;} );"

	console.log('START')
	//On execute la commande dans un "nouveau contexte nodejs" qu'on attend complètement.
	var sortie = execSync("node -e \"" + commandeParamEnVar + "\"").toString();
	//var sortie = execSync("node -e \"" + "console.log('toto')" + "\"").toString();
	console.log(sortie) //Juste du debug pour voir la sortie
	
	console.log('STOP')
	
	// ======== Solution 1 : executer un script situé ailleurs (note ! Attention, il faut potentiellement lui passer des paramètres !) ======== 
}


module.exports = {executeCommandeSync:executeCommandeSync,sanitizeStringSmallQuotes:sanitize} //ici on exporte deux fonctions, on les mets donc dans un seul objet.




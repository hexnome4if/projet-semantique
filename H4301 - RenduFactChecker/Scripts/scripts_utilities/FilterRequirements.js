var synonymesAntonymes = require("./SynonymesAntonymes.js")

function synonymesOf(parsedSentences){
	var synonymes=[]
	for(var i=0 ; i < parsedSentences.length;i++)
	{
		var synonymesTab =synonymesAntonymes.synonyms(parsedSentences[i])
		
		for(var j=0 ; j < synonymesTab.length;j++)
		{	
			synonymes.push(synonymesTab[j])
		}
	}
	return synonymes;
}


function grapheFilter(synonymes,graphe){
	for(var j=0; j<synonymes.length; j++)
	{
		if (typeof synonymes[j] !== 'undefined'){
			
			for(var i=0;i<graphe.length;i++){
			
				if (typeof graphe[i]["p"] !== 'undefined'){
			
					if(synonymes[j].includes(graphe[i]["p"]) ||   graphe[i]["p"].includes(synonymes[j]) ){
						
						delete graphe[i]["p"]
					}
				}
			}
		}
	}
	return graphe
}

module.exports = {synonymesOf:synonymesOf,grapheFilter:grapheFilter}
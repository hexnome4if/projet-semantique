var cheminAccesScripts = "scripts_utilities/"




function allOfData(word) {
	
	var tcom = require('thesaurus-com');
 
	return tcom.search(word);
}

function synonyms(word) {
	
	var tcom = require('thesaurus-com');
 
	return tcom.search(word)["synonyms"];
}

function antonyms(word) {
	
	var tcom = require('thesaurus-com');
 
	return tcom.search(word)["antonyms"];
}


module.exports = {allOfData:allOfData,synonyms:synonyms,antonyms:antonyms} //ici on exporte deux fonctions, on les mets donc dans un seul objet.
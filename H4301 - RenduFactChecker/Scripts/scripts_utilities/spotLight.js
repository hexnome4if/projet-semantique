var cheminAccesScripts = "scripts_utilities/"

var apiSpotlight = require('dbpedia-spotlight'); // See https://www.npmjs.com/search?q=spotlight%20dbpedia&page=1&ranking=optimal
var execSync = require('child_process').execSync;

var debug = false
var debugVerbeux = false

/* ======================= RÃ©cupÃ©ration de triplets ASYNC ======================= */
function mainSpotlight(text, cb) {
	annoter(text, cb);
}

function annoter(textToAnnoter, cb){
	//fix to a specific endpoint (i.e. disabling language detection) 
	apiSpotlight.fixToEndpoint('english');
	//use custom endpoints 
	apiSpotlight.configEndpoints(
	    {
	      "english": {
	      protocol:'http:',
	      host:'model.dbpedia-spotlight.org',
	      path:'/en/annotate',
	      port:'80',
	      confidence:0.2
	      }
	    }
	);
	//RÃ©cupÃ©rer les singulets 
	try {
		apiSpotlight.annotate(textToAnnoter,function (jsonAnnotations) {miseEnForme(jsonAnnotations, cb);} );
	} catch (e) {
	   // les instructions utilisées pour gérer les
	   // exceptions
	   console.log("erreur rencontrée");
	   //logErreurs(e); // on transfère l'objet de l'exception à une méthode 
		          // gestionnaire
	} finally {
	    console.log("finally launched");
	}

}	

/* ======================= UTILITAIRE ASYNC======================= */

//Fonction pour transformer un "gros" json avec beaucoup d'informations en un json avec les informations qu'on dÃ©sire
function miseEnForme(jsonAnnotations, cb){
  //output.response.Resources est la liste des ressources renvoyÃ©es par l'API web
  var res = jsonAnnotations.response.Resources;
  var valOutput = [];
  
  //Les informations qu'on veut retirer de l'objet
  var ensembleValuesToParse = ['@URI','@types','@surfaceForm']
  
  for (var i in res) { //Pour chaque EntitÃ© retournÃ©e
      	valOutput[i] = {} //Initialisation de l'objet
  	for (var j in ensembleValuesToParse) { //Pour chaque propriÃ©tÃ© qu'on veut extraire
  		valOutput[i][ensembleValuesToParse[j]] = res[i][ensembleValuesToParse[j]] //On prend la valeur URI et on la range 'Ã  la main'. Notez la bidouille Ã  cause du caractÃ¨res spÃ©cial
  	}
  }

  cb(valOutput);
}

/* ======================= RÃ©cupÃ©ration de triplets SYNC ======================= */
function mainSpotlightSync(text,confidence,support) {
	var tableauEntite = annoterSync(text,confidence,support);
	
	console.log("Entités retournée suite à la recherche : \n")
	for(let value of tableauEntite){
	    console.log(value)
	}
}

var LONGUEUR_MAX_TEXT_AVANT_REFUS_SITE = 600

function annoterSync(texte,confidence,support){

	if(debug){console.log("Demande de parsing de :")}
	if(debug){console.log(texte)}
	if(debug){console.log("Texte de " + texte.length + " caractères")}
	
	var sortie = []
	//Cas où les données sont trop longues pour un seul appel
	if(texte.length>LONGUEUR_MAX_TEXT_AVANT_REFUS_SITE){

		var nbIterations = Math.ceil(texte.length / LONGUEUR_MAX_TEXT_AVANT_REFUS_SITE);
		
		if(debug){console.log("Le texte est plus long que la longueur maximale d'une seule requête.")}
		if(debug){console.log("Découpe en  " + nbIterations + " requêtes")}
		
		for( var i = 0 ; i<nbIterations ; i++){
			//On découpe le texte en chunks plus petits
			var texteTMP =  texte.substr(0,LONGUEUR_MAX_TEXT_AVANT_REFUS_SITE) //On prend le premier chunk
			texte =  texte.substr(LONGUEUR_MAX_TEXT_AVANT_REFUS_SITE,) //On garde le reste de la string
			
			if(debugVerbeux){console.log("> Requête N°" + i + " avec le texte :")}
			if(debugVerbeux){console.log(texteTMP)}
			if(debugVerbeux){console.log("> Texte pour les requête suivantes :")}
			if(debugVerbeux){console.log(texte)}
			if(debugVerbeux){console.log("\n\n")}
			
			if(texteTMP != ""){
				//On fait une requête pour chaque chunk
				var sortieTMP = execSync('TEXT="' + sanitize(texteTMP) + '" CONFIDENCE=' + confidence + ' SUPPORT=' + support + ' ./' + cheminAccesScripts + 'spotlightText.sh').toString();
				
				if(debugVerbeux){console.log("Le texte considéré est parsable (non vide)")}
				if(debugVerbeux){console.log("La sortie brute de requête est :")}
				if(debugVerbeux){console.log(sortieTMP)}
				
				//On merge les résultats
				sortieTMP = getURIList(sortieTMP); //On ne récupère que les URIs du retour
				
				if(debug){console.log("Sous forme de liste d'URIs :")}
				if(debug){console.log(sortieTMP)}
				
				sortie = sortie.concat(sortieTMP)
				
				if(debugVerbeux){console.log("Sortie totale concaténée :")}
				if(debugVerbeux){console.log(sortie)}
				
			} else {
				if(debug){console.log("Le texte considéré est non-parsable (vide)")}
			}
		}
		
	} else {
		if(texte != ""){
			//TEXT="Donald Trump, né le 14 jui... télévision et homme politique" CONFIDENCE=0.3 SUPPORT=10 ./spotlightText.sh
			sortie = execSync('TEXT="' + sanitize(texte) + '" CONFIDENCE=' + confidence + ' SUPPORT=' + support + ' ./' + cheminAccesScripts + 'spotlightText.sh').toString();
			
			if(debug){console.log("Le texte considéré est parsable (non vide)")}
			if(debug){console.log("La sortie brute de requête est :")}
			if(debug){console.log(sortie)}
			
			sortie = getURIList(sortie); //On ne récupère que les URIs du retour
		} else {
				if(debug){console.log("Le texte considéré est non-parsable (vide)")}
		}
	}

	return sortie;
}

function sanitize(string){
	//Retire les guillemets
	/*
	var newString = string.toString().replace(/\"/g, "")
	var newString = newString.toString().replace(/\(/g, "")
	var newString = newString.toString().replace(/\)/g, "")
	var newString = newString.toString().replace(/\;/g, "")
	var newString = newString.toString().replace(/$/g, "")
	var newString = newString.toString().replace(/\`/g, "")
	var newString = newString.toString().replace(/\“/g, "")
	var newString = newString.toString().replace(/\”/g, "")
	var newString = newString.toString().replace(/\?/g, "")
	var newString = newString.toString().replace(/\‘/g, "")
	var newString = newString.toString().replace(/\’/g, "")
	var newString = newString.toString().replace(/\,/g, "")
	*/
	//Plus simple : On garde que (^) les lettres, les underscores (w) et les whites spaces (s), sans prendre en compte la casse (gi)
	var newString = string.replace(/[^\w\s]/gi, '')

	if(debugVerbeux){console.log("> Texte avant sanitization : ")}
	if(debugVerbeux){console.log(string)}
	if(debugVerbeux){console.log("> Texte après sanitization : ")}
	if(debugVerbeux){console.log(newString)}
		
	return newString
}

//Récupère les URIs depuis un json de retour de Spotlight
function getURIList(json){
	var result = []
	
	try{
		//Sinon, on est en train de traiter du HTML ! 
		if(json.indexOf('<') == -1 ){
			json = JSON.parse(json)

			if(debugVerbeux){console.log("== Affichage du json ==")}
			if(debugVerbeux){console.log(json)}
			if(debugVerbeux){console.log(json['Resources'])}
			if(debugVerbeux){console.log(json['Resources'].length)}

	
			if(json['Resources'] == undefined || json['Resources'].length == 0){
				if(json['Resources'] == undefined){
					//Le JSON est cassé, donc on renvoit une liste vide mais on le dit.
					console.error(">> ATTENTION : JSON DE RETOUR INCORRECTE <<")
				} else if (json['Resources'].length == 0){
					//Le JSON est vide. Pas d'URIS à renvoyer
					console.error(">> ATTENTION : JSON DE RETOUR SANS URIS <<")
				}

			} else {
				for(var i = 0; i< json['Resources'].length ; i++){
					result[i] = json['Resources'][i]["@URI"]
				}
			}
		}
	} catch (e) {
		console.error(e)
	} finally {
		return result
	}

	
	return result
}



module.exports = {getEntitiesSync:annoterSync,getEntitiesSyncVerbeux:mainSpotlightSync,getEntities:mainSpotlight} //ici on exporte deux fonctions, on les mets donc dans un seul objet.
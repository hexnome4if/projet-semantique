var synonymesAntonymes = require("./SynonymesAntonymes.js")
var filteRequirements = require("./FilterRequirements.js")
var Lemmatizer = require("javascript-lemmatizer");

function lemmatize(sentence){
			
		var lemmatizer = new Lemmatizer();
		
		sentence = sentence.replace(/[,;:!\?]/g, "");
		
		sentence = sentence.replace(/ +/g, " ");
		
		var words = sentence.toLowerCase().split(" ");
		
		var newSentence = [];
		
		for(var word of words) {
			
			var lemmas = lemmatizer.lemmas(word,  'verb');
			
			if(lemmas.length > 0 && lemmas[0][1] != "") {
				newSentence.push(lemmas[0][0]);
			}
			
		}
		
		return newSentence;
		
	}




function filter(graphe, sentences){
	var parsedSentences = lemmatize(sentences)
	var synonymes = filteRequirements.synonymesOf(parsedSentences)
	//result = filtredGraphe(synonymes,graphe)
	var predicats=[]
	
	for(var i =0 ; i<graphe.length ; i++){
		predicats.push(graphe[i]["p"]) 
	}
	var resultPredicat = predicats.filter((n) => synonymes.includes(n))
	
	for(var i =0 ; i<graphe.length ; i++){
		if(!resultPredicat.includes(graphe[i]["p"])){
			delete graphe[i]
		}
	}
	graphe = graphe.filter(v=>v!='')
	return graphe;
}

module.exports = {filter:filter}
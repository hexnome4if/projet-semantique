var cheminAccesScripts = "scripts_utilities/"

/* ======================= IMPORTS ======================= */
// Importation des librairaires pour l'execution de code système (linux, bash)
var exec = require('child_process').exec;
var execSync = require('child_process').execSync;

/* ======================= URLS depuis RECHERCHE GOOGLE ======================= */
function mainProperties(URI,format) {
	var jsonProperties = execProperties(URI,format);
	
	console.log("Liste propriétés une entité : \n")
	JSON.stringify(jsonProperties,null,4)
	console.log(jsonProperties)
	
	return jsonProperties
}

function execProperties(URI,format) {
//URI=http://dbpedia.org/resource/Donald_Trump OUTPUT_FORMAT=json ./dbpediaExplore.sh.
	console.log("===== Recherche de : " + URI + " en format : " + format)
	var sortie = execSync("URI=" + URI + " OUTPUT_FORMAT=" + format + " ./" + cheminAccesScripts + "dbpediaExplore.sh").toString();
	
	return callBackProperties('' , sortie , '');
}

function callBackProperties(error, stdout, stderr) {
	result = stdout.split("\n"); //! Dernière case vide ! 
	return result;
}

module.exports = {getProperties:mainProperties} //ici on exporte deux fonctions, on les mets donc dans un seul objet.
var cheminAccesScripts = "scripts_utilities/"

//Ajoute "l'interface" en important un objet javascript, contenu dans le fichier noté.
var searchGoogle = require("./" + cheminAccesScripts + "SearchGoogle.js")
var dereferenceURL = require("./" + cheminAccesScripts + "dereferenceurURL.js")
var spotLight = require("./" + cheminAccesScripts + "spotLight.js")

// ================ UTILITIES ================
function replaceSpace(string){
	var newString = string.toString().replace(/ /g, "+")
	return newString
}

// Operations sur les sets
Set.prototype.isSuperset = function(subset) {
  for (var elem of subset) {
    if (!this.has(elem)) {
      return false;
    }
  }
  return true;
}

Set.prototype.union = function(setB) {
  var union = new Set(this);
  for (var elem of setB) {
    union.add(elem);
  }
  return union;
}

Set.prototype.intersection = function(setB) {
  var intersection = new Set();
  for (var elem of setB) {
    if (this.has(elem)) {
      intersection.add(elem);
    }
  }
  return intersection;
}

Set.prototype.difference = function(setB) {
  var difference = new Set(this);
  for (var elem of setB) {
    difference.delete(elem);
  }
  return difference;
}

//Fonction de filtre de liste
function texteNonVide(element) {
	return element != "";
}

// ================ Paramètres et lancement ================

var queryString = "Barry White is a singer"
var nbEchantillonage = 100
var CONFIDENCE = "0.7"
var SUPPORT = "10"
var debug = false
var debugVerbeux = false
var LONGUEUR_AFFICHAGE_PREVIEW_TEXTE_SITE = 150

var valeurVerite = queryVerite(queryString,nbEchantillonage,CONFIDENCE,SUPPORT)

// ================ IN : Une phrase / OUT : une valeur de sortie ================

function queryVerite(fact,nbEchantillonage,CONFIDENCE,SUPPORT){
	//On remplace les espaces par des + pour préparer la requête google
	fact = replaceSpace(fact)

	if(debug){console.log("Recherche de : " + queryString)}
	if(debug){console.log("Echantillonnage sur : " + nbEchantillonage + " sites")}

	// ======== IN : Une phrase / OUT : Un ensemble d'URL ========
	var listeURL = searchGoogle.getnbEchantillonageURLfromSearch(fact,nbEchantillonage) //Recherche et offset

	if(debug){console.log("URLs retournées :\n")}
	if(debug){for(let value of listeURL){console.log(value)}}

	var texteSites = []
	// ======== IN : Une URL / OUT : Le texte de le page ========
	for(let i in listeURL){

		//Récupération du texte à l'URL et stockage dans une tableau
		texteSites[i] = dereferenceURL.getTextFromURL(listeURL[i]);

		if(debug){console.log("Texte obtenu de : " + listeURL[i] +
			"\n> "  + texteSites[i].toString().substr(0,LONGUEUR_AFFICHAGE_PREVIEW_TEXTE_SITE) + " ... ")};
		if(debugVerbeux){console.log(texteSites[i].toString())}; //Affichage du texte complet
	}

	//On filtre la liste pour éviter les valeur "vides"
	var texteSites = texteSites.filter(texteNonVide);

	//On cherche le set d'entité de la query initiale. Une fois fait, on continue.
	var EntiteTabQuery = spotLight.getEntitiesSync(queryString,CONFIDENCE,SUPPORT);

	if(debug){console.log("Les URIs initiales: " )};
	if(debug){console.log(EntiteTabQuery)};

	//On le transforme en set
	var ensembleInitial = new Set(EntiteTabQuery)
	ensembleInitial.delete("") //Nettoyage du set

	if(debug){console.log("Ensemble initial : " )};
	if(debug){console.log(ensembleInitial)};

	var tabValeurMatching = []
	var nbSitesExplores = texteSites.length //Le nombre de texte nous donne notre "range" d'exploration
	for(let i in texteSites){
		console.log("Traitement du site N°" + i)
		// ======== IN : Un texte / OUT : une liste d'entité ========
		if(debug){console.log("Demande à spotlight de traiter le texte: " +
			"\n> "  + texteSites[i].toString().substr(0,LONGUEUR_AFFICHAGE_PREVIEW_TEXTE_SITE) + " ... ")};
		if(debugVerbeux){console.log(texteSites[i].toString())}; //Affichage du texte complet

		var entitesCourant = spotLight.getEntitiesSync(texteSites[i].toString(),CONFIDENCE,SUPPORT)

		if(debug){console.log("Entités retournées :")}
		if(debug){console.log(entitesCourant)};

		//Transformation en set
		var ensembleCourant = new Set(entitesCourant)

		if(ensembleCourant.size != 0){
			//Un résultat a été envoyé = il y avait quelques chose sur la page
			// ======== IN : Une liste d'entité / OUT : une valeur de match (confiance) avec la recherche initiale, par site ========
			//var intersecCourant = ensembleInitial.intersection(ensembleCourant) // Note : pourrait être fait avec une intersection
			var soustractionCourant = ensembleInitial.difference(entitesCourant) //  équivalent à : entitesCourant - entitesCourant
			if(debug){console.log("Les entitées non présentes dans la page n°" + i + " sont ")}
			if(debug){console.log(soustractionCourant)}

			if(soustractionCourant.size == 0 ){
				//Toutes les entités de base ont été retrouvées dans la page
				tabValeurMatching[i] = 1;
			} else {
				//Des entités n'ont pas été trouvées
				tabValeurMatching[i] = 0;
			}

			if(debug){console.log("Valeur de confiance numéro " + i + " est " + tabValeurMatching[i])};
		} else {
			//Il n'y a vait rien sur la page.
			//On ignore tout le processus
			if(debug){console.log("La page n'a pas renvoyée de résultats URIs. On l'ignore." )};
		}


	}
	var sumTotale = tabValeurMatching.reduce(function(a, b) {
		return a + b;
	}, 0);
	//Fait juste une somme. Si besoin : https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/reduce

	var resultat = sumTotale/nbSitesExplores;

	if(debug){console.log("Résultat final de confiance :" + resultat)};
	return resultat
}

module.exports = {queryVerite:queryVerite}

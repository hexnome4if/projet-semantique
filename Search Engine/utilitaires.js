// Importation des librairaires pour l'execution de code système (linux, bash)
var util = require('util')

function replaceSpace(string){
	var newString = string.toString().replace(/ /g, "+")
	return newString
}

//Pour tester : > console.log(average([0,1,2,3,4,5,6,7,8,9,10]))
function median(tableau){
	var lowMiddle = Math.floor((tableau.length - 1) / 2);
	var highMiddle = Math.ceil((tableau.length - 1) / 2);
	var median = (tableau[lowMiddle] + tableau[highMiddle]) / 2;
	return median
}

//Pour tester : > console.log(average([0,1,2,3,4,5,6,7,8,9,10]))
function average(tableau){
	let sum = tableau.reduce((previous, current) => current += previous);
	let avg = sum / tableau.length;
	return avg
}

/* NOTE : il existe aussi : 
console.warn()
console.info()
*/

module.exports = {average:average, median:median, replaceSpace:replaceSpace} //ici on exporte deux fonctions, on les mets donc
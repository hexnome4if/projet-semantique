var cheminAccesScripts = "scripts_utilities/"

//Ajoute "l'interface" en important un objet javascript, contenu dans le fichier noté.
var searchGoogle = require("./" + cheminAccesScripts + "SearchGoogle.js")
var dereferenceURL = require("./" + cheminAccesScripts + "dereferenceurURL.js")
var spotLight = require("./" + cheminAccesScripts + "spotLight.js")
var proprietes = require("./" + cheminAccesScripts + "getProperties.js")


// ================ UTILITIES ================ 
function replaceSpace(string){
	var newString = string.toString().replace(/ /g, "+")
	return newString
}

//Pour tester : > console.log(average([0,1,2,3,4,5,6,7,8,9,10]))
function median(tableau){
	var lowMiddle = Math.floor((tableau.length - 1) / 2);
	var highMiddle = Math.ceil((tableau.length - 1) / 2);
	var median = (tableau[lowMiddle] + tableau[highMiddle]) / 2;
	return median
}

//Pour tester : > console.log(average([0,1,2,3,4,5,6,7,8,9,10]))
function average(tableau){
	let sum = tableau.reduce((previous, current) => current += previous);
	let avg = sum / tableau.length;
	return avg
}

// ================ Paramètres et lancement ================ 

var queryString = "michelle Obama"
var offset = "0"
var debug = true
var LONGUEUR_AFFICHAGE_PREVIEW_TEXTE_SITE = 150

var valeurVertie = queryVerite(queryString,offset,debug)

// ================ IN : Une phrase / OUT : une valeur de sortie ================

function queryVerite(fact){
	//On remplace les espaces par des + pour préparer la requête google
	fact = replaceSpace(fact)
	
	if(debug){console.log("Recherche de : " + queryString)}
	if(debug){console.log("Offset des URLs retournées : " + offset + "\n")}
	
	// ======== IN : Une phrase / OUT : Un ensemble d'URL ========
	var listeURL = searchGoogle.getURLfromSearch(fact,offset) //Recherche et offset
	
	if(debug){console.log("URLs retournées :\n")}
	if(debug){for(let value of listeURL){console.log(value)}}
	
	// ======== IN : Une URL / OUT : Le texte de le page ========
	var texteSites = []
	for(let i in listeURL){
		//if(debug){console.log("Déréférencement de : " + listeURL[i])}
		
		//Récupération du texte à l'URL et stockage dans une tableau
		texteSites[i] = dereferenceURL.getTextFromURL(listeURL[i]);
		
		if(debug){console.log("Texte obtenu de : " + listeURL[i] + 
			"\n> "  + texteSites[i].toString().substr(0,LONGUEUR_AFFICHAGE_PREVIEW_TEXTE_SITE) + " ... ")};
	}
	
	// ======== IN : Un texte / OUT : Une valeur de confiance ========
	if(debug){if(texteSites.length != listeURL.length){console.log("Attention ! La longueur du tableau texte et URL ne correspondent pas !")}}
		
	var valTrustListe = []
	for(let i in texteSites){
		//if(debug){console.log("Calcul du texte de la page : " + listeURL[i])}
		
		// APPEL à ta lib = A REMPLACER => LE '0.5'
		valTrustListe[i] = 0.5
		/* THIBAULT ? Il faut ajouter ici ce que tu veux. Si tu ne veux pas retourner une valeur de confiance, tu peux retourner autre chose. Ou alors, dupliquer ton script en faisant différents calculs à la fin.Donc potentiellement dédoubler le script pour chaque "logique" : un script de lancement et de calcul pour la connexité, un pour la division simple, ... etc.*/ 
		
		if(debug){console.log("Valeur de confiance de : " + listeURL[i] + " est " + valTrustListe[i])}
	}
	
	
	// ======== IN : une liste de valeur de confiance / OUT : Une valeur de confiance unique totale ========
	var val = 0
	
	val = average(valTrustListe)
	val = median(valTrustListe)
	
	if(debug){console.log("==> Valeur de confiance finale est : " + val + " <==")}
	
	return val
}

module.exports = {queryVerite:queryVerite} 
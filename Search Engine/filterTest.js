var filter = require("./scripts_utilities/filter.js")



var data =[ { s: 'Barack_Obama', p: 'wikiPageWikiLink', o: 'Michelle_Obama' },
  { s: 'Barack_Obama', p: 'spouse', o: 'Michelle_Obama' },
  { s: 'Michelle_Obama', p: 'wikiPageWikiLink', o: 'Barack_Obama' },
  { s: 'Michelle_Obama', p: 'president', o: 'Barack_Obama' },
  { s: 'Michelle_Obama', p: 'spouse', o: 'Barack_Obama' },
  { s: 'Paris', p: 'rdf-schema#seeAlso', o: 'Paris' },
  { s: 'Barack_Obama', p: 'live', o: 'Paris' } ]

var sentences="Barack Obama is husband of Michelle Obama and Michelle lived in Paris"

console.log("------------------")
console.log(sentences)

for(var i =0;i<data.length;i++){
	data[i]["p"]=data[i]["p"].substring( data[i]["p"].lastIndexOf("/") +1) 
	data[i]["s"]=data[i]["s"].substring( data[i]["s"].lastIndexOf("/") +1) 
	data[i]["o"]=data[i]["o"].substring( data[i]["o"].lastIndexOf("/") +1) 
}

console.log(data)
console.log("------------------")
console.log("------------------")


console.log(filter.filter(data, sentences))
/*[ { s: 'Barack_Obama', p: 'spouse', o: 'Michelle_Obama' },
  { s: 'Michelle_Obama', p: 'spouse', o: 'Barack_Obama' },
  { s: 'Barack_Obama', p: 'live', o: 'Paris' } ]
 */
 
console.log("------------------")
console.log("------------------")





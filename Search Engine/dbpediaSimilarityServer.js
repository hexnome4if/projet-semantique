var express = require('express');
var app = express();
var http = require('http').Server(app);
var { spawn } = require('child_process');
var io = require('socket.io')(http);
var DBpediaSimilarity = require("./DBpediaSimilarity.js");

app.use('/', express.static(__dirname + '/webapp'));

app.get('/n', function(req, res){
	res.sendFile(__dirname + '/n.html');
});

io.on('connection', function(socket){
	
	socket.on('app', function(message){
		
		new DBpediaSimilarity(message.request, function(signal, value) {
			
			socket.emit("app", {
				session : message.session,
				signal : signal,
				content : value
			});
			
		});
		
		socket.emit("app", {
			session : message.session,
			signal : "Start",
			content : null
		});		
		
		
	});
	
	
}); 

http.listen(3000, function(){
  console.log('Game server listening on port 3000');
});


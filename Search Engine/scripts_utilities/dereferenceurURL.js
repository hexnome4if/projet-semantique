var cheminAccesScripts = "scripts_utilities/"

/* ======================= IMPORTS ======================= */
// Importation des librairaires pour l'execution de code système (linux, bash)
var exec = require('child_process').exec;
var execSync = require('child_process').execSync;

var LIMIT_INF_TAILLE_TEXT = 25
function countWords(str) {
  return str.trim().split(/\s+/).length; //Note : on retire les espaces avant le premier mot et après le dernier mot, avec trim
}

/* ======================= Récupération du texte du site ======================= */
function mainDereference(URL) {
	var tableauURL = dereferenceurURL(URL);
	
	console.log("URL retournée suite à la recherche : \n")
	for(let value of tableauURL){
	    console.log(value)
	}
}

//Exemple : URL=https://www.whitehouse.gov/administration/first-lady-michelle-obama ./dereferenceURL.sh
function dereferenceurURL(URL){
	//DEBUG // console.log("Déréférencement de : " + URL)
	var sortie = execSync("URL=\"" + URL + "\" ./" + cheminAccesScripts + "dereferenceURL.sh").toString();
	return callBackDereferencement('', sortie, '');
}

//Fonction pour lancer les 
function callBackDereferencement(error, stdout, stderr) {
	result = stdout.split("\n"); //On sépare le texte sur les sauts de ligne
	
	for (var i in result) {
		result[i] = result[i].trim()
		if(countWords(result[i]) < LIMIT_INF_TAILLE_TEXT){
			result.splice(i,1) // On retire l'élément si il est plus petit que la taille minimal de mot requis.
		}
	}
	//DEBUG // Afficher toutes les lignes parsées // console.log(result)
	return result;
}

module.exports = {getTextFromURL:dereferenceurURL,getTextFromURLverbeux:mainDereference} //ici on exporte deux fonctions, on les mets donc dans un seul objet.
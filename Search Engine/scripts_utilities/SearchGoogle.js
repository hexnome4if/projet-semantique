var cheminAccesScripts = "scripts_utilities/"

/* ======================= IMPORTS ======================= */
// Importation des librairaires pour l'execution de code système (linux, bash)
var exec = require('child_process').exec;
var execSync = require('child_process').execSync;


//Retirer les lignes du tableau où la string est égale à ""
function retirerStringVides(resultat){
	for (var i in resultat) {
		if(resultat[i] == "" ){
			resultat.splice(i,1) // On retire l'élément si il est plus petit que la taille minimal de mot requis.
		}
	}
	return resultat
}

/* ======================= URLS depuis RECHERCHE GOOGLE ======================= */
function mainGoogle(params,offset) {
	var tableauURL = execRechercheGoogle(params,offset);
	
	console.log("URL retournée suite à la recherche : \n")
	for(let value of tableauURL){
	    console.log(value)
	}
}

//Fonction pour executer une recherche Google et récupérer des URLS
function execRechercheGoogle(params,offset) {
	var sortie = execSync("QUERY=" + params + " OFFSET=" + offset + " ./" + cheminAccesScripts + "queryGoogle.sh").toString();
	// DEBUG // console.log(sortie)
	return callBackRerchercheGoogle('' , sortie , '');
}

function execRechercheGoogleNURLS(params,nbResultats){

	//On calcul le nombre de recherche qu'on devra faire (en multiple de 10)
	var nbRecherche = nbResultats/10
	offset = 0
	var resultat = []
	
	//console.log(nbRecherche)
	for(var i = 0; i<nbRecherche; i++){
		//On fait la recherche
		var TMP = execRechercheGoogle(params,offset)
		//On rajoute les résultats à notre tableau résultat
		resultat = resultat.concat(TMP)
		//On ajoute l'offset du nombre de résultat déjà obtenus précédemments.
		offset = offset+10
	}
	
	//On retire les lignes avec des urls égales à ""
	resultat = retirerStringVides(resultat)
	
	return resultat
}

//Fonction pour lancer les déréférencement à partir de des URLs de la recherche
function callBackRerchercheGoogle(error, stdout, stderr) {
	//DEBUG // Afficher directement la sortie donnée à la fonction // console.log(stdout)
	result = stdout.split("\n"); //! Dernière case vide ! 
	//DEBUG // Afficher toutes les lignes parsées // console.log(result)

	return result;
}

module.exports = {getURLfromSearch:execRechercheGoogle,getURLfromSearchVerbeux:mainGoogle,getnbEchantillonageURLfromSearch:execRechercheGoogleNURLS} //ici on exporte deux fonctions, on les mets donc dans un seul objet.
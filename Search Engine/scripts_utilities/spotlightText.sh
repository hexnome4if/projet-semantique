#!/bin/bash

: ${TEXT?"Please precise TEXT by adding TEXT=your_text_to_annotate"}

OUTPUT_FORMAT=${OUTPUT_FORMAT:-json}
CONFIDENCE=${CONFIDENCE:-0.35}
L=${L:-"en"}

curl http://model.dbpedia-spotlight.org/$L/annotate  \
  --data-urlencode "text=$TEXT" \
  --data "confidence=$CONFIDENCE" \
  -H "Accept: application/$OUTPUT_FORMAT" 2>/dev/null 

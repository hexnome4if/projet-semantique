//Ajoute "l'interface" en important un objet javascript, contenu dans le fichier noté.
var module1 = require("./partie1.js")
var moduleUtils = require("./utilitaires.js")

moduleUtils.print("==== Execution d'une recherche Google ==== \n")
var queryString = "michelle+Obama"
var offset = "0"

moduleUtils.print("Recherche de : " + queryString)
moduleUtils.print("Offset des URLs retournées : " + offset + "\n")

module1.getURLfromSearch(queryString,offset) //Recherche et offset

var apiSpotlight = require('dbpedia-spotlight'); // See https://www.npmjs.com/search?q=spotlight%20dbpedia&page=1&ranking=optimal
var async = require("async");

/* ======================= Récupération de triplets ======================= */
function mainSpotlight(text, cb) {
	annoter(text, cb)
}

function annoter(textToAnnoter, cb){
	//fix to a specific endpoint (i.e. disabling language detection) 
	apiSpotlight.fixToEndpoint('english');
	//use custom endpoints 
	apiSpotlight.configEndpoints(
	    {
	      "english": {
	      protocol:'http:',
	      host:'model.dbpedia-spotlight.org',
	      path:'/en/annotate',
	      port:'80',
	      confidence:0.2
	      }
	    }
	);

	function test(jsonAnnotations, val) {
		val = miseEnForme(jsonAnnotations, cb);
		console.log("IN ")
		console.log(val)
	}
	
	//Récupérer les singulets 
	apiSpotlight.annotate(textToAnnoter,function (jsonAnnotations) {
		val = miseEnForme(jsonAnnotations, cb);
	} );

	
	return val
}

//Fonction pour transformer un "gros" json avec beaucoup d'informations en un json avec les informations qu'on désire
function miseEnForme(jsonAnnotations, cb){
  //output.response.Resources est la liste des ressources renvoyées par l'API web
  var res = jsonAnnotations.response.Resources;
  var valOutput = [];
  
  //Les informations qu'on veut retirer de l'objet
  var ensembleValuesToParse = ['@URI','@types','@surfaceForm']
  
  for (var i in res) { //Pour chaque Entité retournée
      	valOutput[i] = {} //Initialisation de l'objet
  	for (var j in ensembleValuesToParse) { //Pour chaque propriété qu'on veut extraire
  		valOutput[i][ensembleValuesToParse[j]] = res[i][ensembleValuesToParse[j]] //On prend la valeur URI et on la range 'à la main'. Notez la bidouille à cause du caractères spécial
  	}
  }
  
  return valOutput
  //cb(valOutput);
}


module.exports = {getEntities:mainSpotlight} //ici on exporte deux fonctions, on les mets donc dans un seul objet.
//Ajoute "l'interface" en important un objet javascript, contenu dans le fichier noté.
var searchGoogle = require("./SearchGoogle.js")
var dereferenceURL = require("./dereferenceurURL.js")
var spotLight = require("./spotLight.js")
var proprietes = require("./getProperties.js")

function replaceSpace(string){
	var newString = string.toString().replace(/ /g, "+")
	return newString
}

// ================ Paramètres et lancement ================ 

var queryString = "michelle Obama"
var nbEchantillonage = 10
var debug = true
var LONGUEUR_AFFICHAGE_PREVIEW_TEXTE_SITE = 150

var valeurVerite = queryVerite(queryString)

// ================ IN : Une phrase / OUT : une valeur de sortie ================

function queryVerite(fact){
	//On remplace les espaces par des + pour préparer la requête google
	fact = replaceSpace(fact)
	
	if(debug){console.log("Recherche de : " + queryString)}
	if(debug){console.log("Echantillonnage sur : " + nbEchantillonage + " sites")}
	
	// ======== IN : Une phrase / OUT : Un ensemble d'URL ========
	var listeURL = searchGoogle.getnbEchantillonageURLfromSearch(fact,nbEchantillonage) //Recherche et offset
	
	if(debug){console.log("URLs retournées :\n")}
	if(debug){for(let value of listeURL){console.log(value)}}
	
	var texteSites = []
	// ======== IN : Une URL / OUT : Le texte de le page ========
	for(let i in listeURL){
		//if(debug){console.log("Déréférencement de : " + listeURL[i])}
		
		//Récupération du texte à l'URL et stockage dans une tableau
		texteSites[i] = dereferenceURL.getTextFromURL(listeURL[i]);
		
		if(debug){console.log("Texte obtenu de : " + listeURL[i] + 
			"\n> "  + texteSites[i].toString().substr(0,LONGUEUR_AFFICHAGE_PREVIEW_TEXTE_SITE) + " ... ")};
	}
	
	//On cherche le set d'entité de la query initiale. Une fois fait, on continue. Bidouille pour pouvoir gérer l'asynchrone.
	spotLight.getEntities(queryString, function (result,texteSites) { continueQueryVerite(result,texteSites); });
//NOTE : test sans le "texteSites" dans la query au départ
	//.bind(continueQueryVerite, texteSites)
}

function continueQueryVerite(ensembleInitial,texteSites){
	//On a le set d'entité de la query initiale. 
	console.log("Ensemble initial : " )
	console.log(ensembleInitial)
	console.log("text initial : ")
	console.log(texteSites)
	
	var tabValeurMatching = []
	
	//Bidouille pour pouvoir gérer l'asynchrone. On décrément à chaque fois qu'un retour d'entité est reçu
	var todo = texteSites.length;
	
	for(let i in texteSites){
		// ======== IN : Un texte / OUT : une liste d'entité ========
		spotLight.getEntities(texteSites[i], function(results) { 
			if(debug){console.log("Entités retournées :\n" + results)}
			
			// ======== IN : Une liste d'entité / OUT : une valeur de match (confiance) avec la recherche initiale, par site ========
			if(debug){console.log("TEST :\n" + i)}
			
			
			tabValeurMatching[i] = 1;
			
			//Appel de la fin du traitement si c'était la dernière boucle
			todo--;
			if(todo==0){
				finishqueryVerite(tabValeur)
			}
		})
	}
}

function finishqueryVerite(tabValeur){

	console.log(tabValeur)
	// ======== IN : une liste de valeur de confiance / OUT : Une valeur de confiance unique totale ========
	var val = 0
	
	//val = average(valTrustListe)
	//val = median(valTrustListe)
	
	if(debug){console.log("==> Valeur de confiance finale est : " + val + " <==")}
}

module.exports = {queryVerite:queryVerite} 
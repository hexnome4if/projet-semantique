var apiSpotlight = require('dbpedia-spotlight'); // See https://www.npmjs.com/search?q=spotlight%20dbpedia&page=1&ranking=optimal

/* ======================= RÃ©cupÃ©ration de triplets ======================= */
function mainSpotlight(text, cb) {
	annoter(text, cb);
}

function annoter(textToAnnoter, cb){
	//fix to a specific endpoint (i.e. disabling language detection) 
	apiSpotlight.fixToEndpoint('english');
	//use custom endpoints 
	apiSpotlight.configEndpoints(
	    {
	      "english": {
	      protocol:'http:',
	      host:'model.dbpedia-spotlight.org',
	      path:'/en/annotate',
	      port:'80',
	      confidence:0.2
	      }
	    }
	);
	//RÃ©cupÃ©rer les singulets 
	try {
		apiSpotlight.annotate(textToAnnoter,function (jsonAnnotations) {miseEnForme(jsonAnnotations, cb);} );
	} catch (e) {
	   // les instructions utilisées pour gérer les
	   // exceptions
	   console.log("erreur rencontrée");
	   //logErreurs(e); // on transfère l'objet de l'exception à une méthode 
		          // gestionnaire
	} finally {
	    console.log("finally launched");
	}

}	



//Fonction pour transformer un "gros" json avec beaucoup d'informations en un json avec les informations qu'on dÃ©sire
function miseEnForme(jsonAnnotations, cb){
  //output.response.Resources est la liste des ressources renvoyÃ©es par l'API web
  var res = jsonAnnotations.response.Resources;
  var valOutput = [];
  
  //Les informations qu'on veut retirer de l'objet
  var ensembleValuesToParse = ['@URI','@types','@surfaceForm']
  
  for (var i in res) { //Pour chaque EntitÃ© retournÃ©e
      	valOutput[i] = {} //Initialisation de l'objet
  	for (var j in ensembleValuesToParse) { //Pour chaque propriÃ©tÃ© qu'on veut extraire
  		valOutput[i][ensembleValuesToParse[j]] = res[i][ensembleValuesToParse[j]] //On prend la valeur URI et on la range 'Ã  la main'. Notez la bidouille Ã  cause du caractÃ¨res spÃ©cial
  	}
  }

  cb(valOutput);
}


module.exports = {getEntities:mainSpotlight} //ici on exporte deux fonctions, on les mets donc dans un seul objet.
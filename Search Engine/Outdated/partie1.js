/* ======================= IMPORTS ======================= */
// Importation des librairaires pour l'execution de code système (linux, bash)
var exec = require('child_process').exec;

//Importation des fichiers "maisons" nécessairs
var moduleAnalyseSite = require("./AnalyseurDeSite.js")
var moduleAnalyseInformation = require("./AnalyseurInformation.js") 

/* ======================= UTILITAIRES ======================= */
var LIMIT_INF_TAILLE_TEXT = 10

function wordCount(str) { 
  return str.split(" ").length;
}

function countWords(str) {
  return str.trim().split(/\s+/).length; //Note : on retire les espaces avant le premier mot et après le dernier mot, avec trim
}

/* ======================= URLS depuis RECHERCHE GOOGLE ======================= */
//Fonction pour executer une recherche Google et récupérer des URLS
function execRechercheGoogle(params,offset) {
	exec("QUERY=" + params + " OFFSET=" + offset + " ./queryGoogle.sh", callBackRerchercheGoogle);
}

//Fonction pour executer une recherche Google et récupérer des URLS
function execRechercheGoogle2(params,offset, nbresultats) {
	//A FAIRE (?) : si on veut 20 résultats, la fonction doit pouvoir les gérer. On concat les résultats et on les balance à l'affichage standard
}

//Fonction pour lancer les déréférencement à partir de des URLs de la recherche
function callBackRerchercheGoogle(error, stdout, stderr) {
	//DEBUG // Afficher directement la sortie donnée à la fonction // console.log(stdout)
	result = stdout.split("\n"); //! Dernière case vide ! 
	//DEBUG // Afficher toutes les lignes parsées // console.log(result)
	
	console.log("URL retournée suite à la recherche : \n")
	for(let value of result){
	    console.log(value)
	    dereferenceurURL(value)
	}
}

/* ======================= Récupération du texte du site ======================= */
function dereferenceurURL(URL){
//Exemple : URL=https://www.whitehouse.gov/administration/first-lady-michelle-obama ./dereferenceURL.sh
	console.log("\n==== appel de déréférencement pour " + URL + " ====\n")
	console.log(URL)
	exec("URL=" + URL + " ./dereferenceURL.sh", callBackDereferencement);
}

//Fonction pour lancer les 
function callBackDereferencement(error, stdout, stderr) {
	//DEBUG // Afficher directement la sortie donnée à la fonction // console.log(stdout)
	result = stdout.split("\n"); //On sépare le texte sur les sauts de ligne
	
	for (var i in result) {
		result[i] = result[i].trim()
		//DEBUG // Affichage de la ligne et de l'élement du tableau // 	console.log("Evaluating : N°" + i + "/" + countWords(result[i]) + " words =" + result[i]) + "\n";
		if(countWords(result[i]) < LIMIT_INF_TAILLE_TEXT){
			//DEBUG // console.log("====> String too short ! We slice it.\n");
			result.splice(i,1) // On retire l'élément si il est plus petit que la taille minimal de mot requis.
		}
	}
	//DEBUG // Afficher toutes les lignes parsées // console.log(result)
	
	for (var i in result) {
		annoter(result[i])
	}
}
/* ======================= Récupération de triplets ======================= */
var apiSpotlight = require('dbpedia-spotlight'); // See https://www.npmjs.com/search?q=spotlight%20dbpedia&page=1&ranking=optimal


//Fonction pour transformer un "gros" json avec beaucoup d'informations en un json avec les informations qu'on désire
function miseEnForme(jsonAnnotations){
  //DEBUG // Raw output print //  console.log(jsonAnnotations);
  //output.response.Resources est la liste des ressources renvoyées par l'API web
  var res = jsonAnnotations.response.Resources;
  var valOutput = [];
  
  //Les informations qu'on veut retirer de l'objet
  var ensembleValuesToParse = ['@URI','@types','@surfaceForm']
  
  for (var i in res) { //Pour chaque Entité retournée
      	valOutput[i] = {} //Initialisation de l'objet
  	for (var j in ensembleValuesToParse) { //Pour chaque propriété qu'on veut extraire
  		//DEBUG // Affichage durant le parsing // console.log('Parsing de : ' + ensembleValuesToParse[j] + " en num " + j)
  		valOutput[i][ensembleValuesToParse[j]] = res[i][ensembleValuesToParse[j]] //On prend la valeur URI et on la range 'à la main'. Notez la bidouille à cause du caractères spécial
  	}
  }
  // DEBUG // Affichage du json de sortie // 
  console.log(valOutput)
  
  return valOutput
}


function annoter(textToAnnoter){
	//fix to a specific endpoint (i.e. disabling language detection) 
	apiSpotlight.fixToEndpoint('english');
	//use custom endpoints 
	apiSpotlight.configEndpoints(
	    {
	      "english": {
	      protocol:'http:',
	      host:'model.dbpedia-spotlight.org',
	      path:'/en/annotate',
	      port:'80',
	      confidence:0.2
	      }
	    }
	);
	//Récupérer les singulets 
	apiSpotlight.annotate(textToAnnoter,miseEnForme);
}	


/* ======================= Création des triplets depuis le texte ======================= */

// ATTENTION ! Il faut qu'un seul export par fichier ! On défini l'interface avec les autres modules.
//module.exports = exectest; //Ici on exporte une seule fonction 
module.exports = {getURLfromSearch:execRechercheGoogle} //ici on exporte deux fonctions, on les mets donc dans un seul objet.
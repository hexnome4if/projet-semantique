var fs = require("fs");
//console.log("la");
var json = fs.readFileSync("Bill Gates is the founder of Microsoft.json").toString();
//console.log("après DT");
var jsonWords = fs.readFileSync("wordBank.json").toString();
//console.log("après bank");
// var json = fs.readFileSync("Donald Trump leads USA.json").toString();
// var json = fs.readFileSync("USA is in North America.json").toString();
// var json = fs.readFileSync("Winston Chruchill leads UK in 1945.json").toString();
// var json = fs.readFileSync("Leonard Da Vinci painted Mona Lisa.json").toString();
// var json = fs.readFileSync("Michelle Obama is married to Barrack Obama.json").toString();
// var json = fs.readFileSync("result.json").toString();

// Exemple de donn�es

// data.query.original : "Donald Trump is the president of the United State of America"
// data.query.lemmatized : " is the president of the United State of America"
// data.query.enhanced : ["be","the","president","of","the","unite","state","of","america","lewis","mitchell","powell","weld", ...]

// data.entities.uris : ["http://dbpedia.org/resource/Donald_Trump","http://dbpedia.org/resource/President_of_the_United_States","http://dbpedia.org/resource/United_States"]
// data.entities.label : ["Donald Trump","President of the United States","United States"]

// data.results[i].triplet.s : "http://dbpedia.org/ontology/abstract"
// data.results[i].triplet.p: "has abstract"
// data.results[i].triplet.pt: "http://www.w3.org/2000/01/rdf-schema#label"
// data.results[i].triplet.o : "The President of the United States of America (POTUS) is the elected head of state and head of government of the United States. The president leads the executive branch of the federal government and is the commander-in-chief of the United States Armed Forces. The President of the United States is considered one of the world's most powerful people, leading the world's only contemporary superpower. The role includes being the commander-in-chief of the world's most expensive military with the largest nuclear arsenal and leading the nation with the largest economy by real and nominal GDP. The office of the president holds significant hard and soft power both in the United States and abroad. Article II of the U.S. Constitution vests the executive power of the United States in the president. The power includes execution of federal law, alongside the responsibility of appointing federal executive, diplomatic, regulatory and judicial officers, and concluding treaties with foreign powers with the advice and consent of the Senate. The president is further empowered to grant federal pardons and reprieves, and to convene and adjourn either or both houses of Congress under extraordinary circumstances. The president is largely responsible for dictating the legislative agenda of the party to which the president is enrolled. The president also directs the foreign and domestic policy of the United States. Since the founding of the United States, the power of the president and the federal government has grown substantially. The president is indirectly elected by the people through the Electoral College to a four-year term, and is one of only two nationally elected federal officers, the other being the Vice President of the United States. The Twenty-second Amendment, adopted in 1951, prohibits anyone from ever being elected to the presidency for a third full term. It also prohibits a person from being elected to the presidency more than once if that person previously had served as president, or acting president, for more than two years of another person's term as president. In all, 43 individuals have served 44 presidencies (counting Cleveland's two non-consecutive terms separately) spanning 56 full four-year terms. On January 20, 2009, Barack Obama became the 44th and current president. On November 6, 2012, he was re-elected and is currently serving the 57th term. The next presidential election is scheduled to take place on November 8, 2016; on January 20, 2017, the newly elected president will take office."

// data.results[i].sentence : "President of the United States has abstract The President of the United States of America (POTUS) is the elected head of state and head of government of the United States"
// data.results[i].matchWords : {"be": 1,"the": 6,"president": 1,"of": 8,"state": 1,"america": 1}
// data.results[i].matchEntities : ["President of the United States", "United States"]

//console.log("avant parsing data");
var data = JSON.parse(json);
//console.log("apres parsing data");
var wordBank = JSON.parse(jsonWords);
//console.log("apres parsing bank");

// Calcul du score total des mots non entités de la fact et stockage des mots de la fact
console.log("--- Partie Banque de données de mots ---");
var scoresMots = [];
var mots = [];
var scoreMot;
for(var i = 0; i < data.query.enhanced.length; i++){
	for(var j = 0; j < wordBank.bank.length; j++){
		if(data.query.enhanced[i] == wordBank.bank[j]){
			scoreMot = (j+1)/wordBank.bank.length;
			scoresMots.push(scoreMot);
			break;
		}
	}
	if(scoresMots.length==i){
		scoreMot = 1
		scoresMots.push(scoreMot);
	}
	console.log("score du mot n°",i," = ",scoreMot);
	mots.push(data.query.enhanced[i]);
}

// Début du scoring de chanque sentence résultat
console.log("--- Partie Etude des resultats ---");
var scoreResult;
var motEtudie;
for(var i = 0; i < data.results.length; i++) {
	console.log();
	console.log("resultat ",i);
	// Work here
	scoreResult = 0;
	for(var j = 0; j < Object.keys(data.results[i].matchWords).length; j++){
		motEtudie=Object.keys(data.results[i].matchWords)[j];
		console.log("Presence du mot : ",motEtudie);
		for(var k = 0; k < mots.length ; k++){
			if(Object.keys(data.results[i].matchWords)[j]==mots[k]){
				scoreResult = scoreResult + scoresMots[k] * data.results[i].matchWords[motEtudie];
				break;
			}
		}
	}

	scoreResult = scoreResult * data.results[i].matchEntities.length;
	console.log("score total du resultat n°",i," = ",scoreResult);
	data.results[i].score = scoreResult;
	data.results[i].fiabilityPercentage = (data.results[i].matchEntities.length)/(data.entities.label.length);
	// End work here
}

data.results.sort(function(a, b) {
  return b.score - a.score; //Tri d�croissant
});

for(var i = 0; i < data.results.length; i++) {

	console.log(data.results[i].score, data.results[i].sentence);
	console.log("");

}

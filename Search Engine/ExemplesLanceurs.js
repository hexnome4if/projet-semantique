var cheminAccesScripts = "scripts_utilities/"

//Ajoute "l'interface" en important un objet javascript, contenu dans le fichier noté.
var searchGoogle = require("./" + cheminAccesScripts + "SearchGoogle.js")
var dereferenceURL = require("./" + cheminAccesScripts + "dereferenceurURL.js")
var spotLight = require("./" + cheminAccesScripts + "spotLight.js")
var proprietes = require("./" + cheminAccesScripts + "getProperties.js")

var moduleUtils = require("./utilitaires.js")

console.log("==== Execution d'une recherche Google ==== \n")
var queryString = "michelle+Obama"
var offset = "0"

console.log("Recherche de : " + queryString)
console.log("Offset des URLs retournées : " + offset + "\n")

searchGoogle.getURLfromSearch(queryString,offset) //Recherche et offset
console.log(searchGoogle.getnbEchantillonageURLfromSearch(queryString,100))

console.log("==== Dereferencement URL ==== \n")
//Exemple : 
var URL = 'https://fr.wikipedia.org/wiki/Michelle_Obama'

dereferenceURL.getTextFromURL(URL) //Recherche et offset

console.log("==== Entities ==== \n")
spotLight.getEntities("Donald Trump est président des Etats-unis", function(results) { 
	console.log(results);
})

console.log("==== Properties ==== \n")
var URIToGetEntities = "http://dbpedia.org/resource/Donald_Trump"
proprietes.getProperties(URIToGetEntities, "json")


//Ajoute "l'interface" en important un objet javascript, contenu dans le fichier noté.
var searchMultisite = require("./searchMultisite.js")
var searchNaturelle = require("./searchNaturelle.js")

var factInitiale = "Barack Obama est un chat"
var nbEchantillonage = 100
var CONFIDENCE = "0.7"
var SUPPORT = "10"

console.log("Script de recherche basique")

console.log("Script de recherche Naturel")
searchNaturelle.queryVerite(factInitiale,nbEchantillonage,CONFIDENCE,SUPPORT)

console.log("Script de recherche avancé")
searchMultisite.queryVerite(factInitiale)

console.log("Script de recherche avancé avec filtrage")


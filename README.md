# My project's README

Comment l'installer rapidement : 

* (Arch) sudo pacman -S nodejs
* (Arch) sudo pacman -S npm

Comment lancer rapidement un programme js : 

* (Arch) node ./monfichier.js

Clé Alchemy : 

* uKdCU2D3lMbvq5K9pq49OVeBWituwtM-lUjLXY8lIfU7

## Sites utiles : 
La documentation officielle : 

* https://nodejs.org/dist/latest-v8.x/docs/api/os.html

Pour avoir des libs toutes faites (fft, ...) : 

* https://www.npmjs.com

Pour l'installer : 

* https://wiki.archlinux.org/index.php/Node.js_
* npm update packageName

Tuto rapide : 

* https://fr.slideshare.net/reverentgeek/nodejs-crash-course

## Informations :
Pour chercher des informations sur internet, chercher "nodejs <ce que j'ai besoin>" si c'est lié à du système/Serveur